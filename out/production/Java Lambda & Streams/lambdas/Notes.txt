


1. A functional interface has only one method. @FunctinalInterface annot is not mandatory but a good practice.
2. All lambda functions implement a functional interface.
3. Return type is defined in that interface that's why lambdas dont return values in signatures;
4. Compiler will know what lambda is returning in its abstract definition. so not required.

5. We can even not declare the type of input a lambda will take in its implementation
as its already defined.
means say ::
     (integer x , integer y) -> {
     if(x<y){
      return x;
      }

      else {


      return y;

      }
     }



     is equal to  :::::::::::::



      ( x , y) -> {
          if(x<y){
           return x;
           }
           else {

           return y;

           }
          }

     }

     and again equal to ::::::::::

       ( x , y) -> {  return (x<y ? x:y)}



       and also if body contains single stmt, then braces, semicolon and return also  not required.

      means     ( x , y) ->   (x<y ? x:y)

      ---------------------------------------------------------------


      Single Param lambda

      (Integer x) -> { return Integer.toString(x);}
      x -> { return Integer.toString(x);}
      x -> Integer.toString(x)


      ------------------------------------------------------------------------


      No Param lambda

      ( ) -> { println("Hello");}
      ( ) ->  println("Hello")
      here parenthesis are mandatory






