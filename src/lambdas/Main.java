package lambdas;

import  static java.lang.System.out;
public class Main {


    @FunctionalInterface
    interface Converter<T,R> {
        R apply(T source);
    }


    public static void main(String[] args) {
        Converter<String,Boolean> stringBooleanConverter = s -> Boolean.parseBoolean(s);
        out.println(stringBooleanConverter.apply("TRUE"));

        out.println(stringBooleanConverter.apply("tRue"));

        out.println(stringBooleanConverter.apply("False"));

        out.println(stringBooleanConverter.apply("FalSE"));
        out.println(stringBooleanConverter.apply("hey"));
        out.println(stringBooleanConverter.apply(null));




    }
}
