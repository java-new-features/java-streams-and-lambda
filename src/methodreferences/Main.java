package methodreferences;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Main {

    public static void main(String[] args) {

        List<String> persons= new ArrayList<>();
        persons.add("Anshul");
        persons.add("Ankit");
        persons.add("Prajjval");
        persons.add("Pranshul");

       long x= persons.stream()
                .filter(Objects::nonNull)
                .peek(System.out::println)
                .count();





       Sayable sayable=Main::saySomething;
       sayable.say();


    }

    public static void saySomething(){
        System.out.println("Hello, this is static method.");
    }
}

interface Sayable{
    void say();
}

