package streams;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        List<String> dataSource =new ArrayList<>();
        dataSource.add("this");
        dataSource.add("is");
        dataSource.add("a");
        dataSource.add("test");
        dataSource.add("it");
        dataSource.add("is");
        dataSource.add("only");
        dataSource.add("a");
        dataSource.add("test");
        dataSource.add("YIPPEE");

        long count=dataSource.stream()
                .distinct()
                .filter(word -> (word.length() >= 4))
                .count();

        //Another way to write
        long count2=dataSource.stream()
                .distinct()
                .filter(word -> { return word.length() >= 4 ;} )
                .count();

        System.out.println(String.format("Collection has %d distinct words of 4 or More characters",count));


        System.out.println(String.format("Collection has %d distinct words of 4 or More characters",count2));

    }
}
